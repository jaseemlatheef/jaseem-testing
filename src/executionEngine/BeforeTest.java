package executionEngine;

import models.BrowserTypes;
import models.IBeforeTest;
import models.TestScript;

import org.openqa.selenium.By;

public class BeforeTest implements IBeforeTest {

	@Override
	public void executeBeforeTest(TestScript script) {
		if (!script.getSheetName().equals("Login")) {
			script.initializeDriver(BrowserTypes.CHROME);
			login(script);
		}
	}

	private void login(TestScript script) {
;
		script.goToUrl("http://ec2-35-154-21-64.ap-south-1.compute.amazonaws.com");
		script.sendKeys(
				By.xpath("//*[@id=\"auth-cell\"]/div/div[3]/div[2]/form/div[2]/div/div[2]/div[1]/input"),
				"admin@anshin-kyuyu.com");// "");
		script.sendKeys(
				By.xpath("//*[@id=\"auth-cell\"]/div/div[3]/div[2]/form/div[2]/div/div[2]/div[2]/input"),
				"123456");
		script.click(By
				.xpath("//*[@id=\"auth-cell\"]/div/div[3]/div[2]/form/div[2]/div/div[2]/div[3]/div/input"));

	}

}
