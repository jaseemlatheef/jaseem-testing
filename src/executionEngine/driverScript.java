package executionEngine;

import models.TestScript;
import models.TestSuite;

public class driverScript {

	public static void main(String[] args) throws Exception {
		//executeSuite();
		executeScript();
	}

	public static void executeSuite() throws Exception {
		String sPath = "C://Users//jlatheef//workspace//SOCXO Admin//src//dataEngine//DataEnginetest.xlsx";
		TestSuite suite = new TestSuite(sPath, 3, 4, 1, 5);
		suite.beforeEveryTest = new BeforeTest();
		suite.afterEveryTest = new AfterTest();
		suite.execute(System.out);
	}

	public static void executeScript() throws Exception {
		String sPath = "C://Users//jlatheef//workspace//SOCXO Admin//src//dataEngine//DataEnginetest.xlsx";
		String sheetName="signup";
		TestScript script = new TestScript(sPath,sheetName, 2, 3, 1, 10 , 4); 
		//script.beforeTest = new BeforeTest();
		script.afterTest = new AfterTest();
		script.execute();
	}
}
